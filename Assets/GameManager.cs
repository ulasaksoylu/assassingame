using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject panelLevelCompleted;
    public GameObject panelGameOver;

    public enum State {LEVELCOMPLETE, GAMEOVER}
    State _state;

    void Start()
    {

    }
    public void SwitchState(State newState)
    {

    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.LEVELCOMPLETE:
                panelLevelCompleted.SetActive(true);
                break;
            case State.GAMEOVER:
                panelGameOver.SetActive(true);
                break;
        }


    }
    private void Update()
    {
        switch (_state)
        {
            case State.LEVELCOMPLETE:
                break;
            case State.GAMEOVER:
                break;
        }
    }
    void EndState()
    {
        switch (_state)
        {
            case State.LEVELCOMPLETE:
                panelLevelCompleted.SetActive(false);
                break;
            case State.GAMEOVER:
                panelGameOver.SetActive(false);
                break;
        }
    }


}
