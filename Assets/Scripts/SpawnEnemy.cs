using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public GameObject enemy;
    void Start()
    {
        SpawnerEnemy();
    }

    void Update()
    {
    
        //Invoke(nameof(SpawnerEnemy), Time.deltaTime);
        
    }

    void SpawnerEnemy()
    {
        Instantiate(enemy, transform.position, Quaternion.identity);
    }
}
