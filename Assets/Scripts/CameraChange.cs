using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraChange : MonoBehaviour
{
    public Camera _MainCamera;
    public Camera _SecondCamera;
    public Image _PointTarget;
    public Image _SniperTarget;

    void Start()
    {
        _MainCamera = Camera.main;

        _MainCamera.enabled = true;
        _SecondCamera.enabled = false;

        _PointTarget.enabled = true;
        _SniperTarget.enabled = false;

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.CapsLock))
        {
            if (_MainCamera.enabled)
            {
                _SecondCamera.enabled = true;
                _MainCamera.enabled = false;

                _PointTarget.enabled = false;
                _SniperTarget.enabled = true;

            }
            else if (!_MainCamera.enabled)
            {
                _SecondCamera.enabled = false;
                _MainCamera.enabled = true;

                _PointTarget.enabled = true;
                _SniperTarget.enabled = false;

            }
        }
    }





}
