using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReloadObject : MonoBehaviour
{
     public static event Action OnReload;

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            Debug.Log("aaaaaaa");
            if (OnReload != null)
            {
                OnReload();
            }
                       
        }

    }

}
