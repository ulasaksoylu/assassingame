using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunShootScript : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float impactForce = 50f;
    public float fireRate = 15f;
    public Text bulletCountText;
    public int _bulletCount = 20;


    public Camera fpsCam;

    private float nextTimeToFire = 0;

    private void Awake()
    {
        ReloadObject.OnReload += CounterAddBullet;
    }

    private void OnDestroy()
    {
        ReloadObject.OnReload -= CounterAddBullet;
    }

    private void Start()
    {
        CounterBullet();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f/fireRate;
            
            if (_bulletCount>0)
            {
                _bulletCount--;
                CounterBullet();
                Shoot();
            }
            
        }
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);   
            Target target = hit.transform.GetComponent<Target>(); 
            if (target != null)
            {
                target.TakeDamage(damage);
            }

            if (hit.rigidbody != null)
            {

                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            
        }
    }

    public void CounterBullet()
    {
        bulletCountText.text = "Cephane: " + _bulletCount;
    }

    public void CounterAddBullet()
    {
        _bulletCount = _bulletCount + 5;
        bulletCountText.text = "Cephane: " + _bulletCount;
    }
}
